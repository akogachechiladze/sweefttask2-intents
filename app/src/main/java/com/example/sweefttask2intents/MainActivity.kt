package com.example.sweefttask2intents

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.sweefttask2intents.databinding.ActivityMainBinding
import com.example.sweefttask2intents.model.UsersModel

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initListeners()
    }

    private fun initListeners() {
        binding.btnNext.setOnClickListener {
            val firstname = binding.firstname.text.toString()
            val lastname = binding.lastname.text.toString()
            val age = binding.age.text.toString()

            if (firstname.isNotEmpty() && lastname.isNotEmpty() && age.isNotEmpty()) {
//                val user = UsersModel(firstname, lastname, age.toInt())
                Intent(this, MainActivity2::class.java).also {
                    it.putExtra("firstname", firstname)
                    it.putExtra("lastname", lastname)
                    it.putExtra("age", age)

                    //second way
//                    it.putExtra("userModel", user)
                    startActivityForResult(it, 1)
                }
            }
            else {
                Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show()
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                binding.location.text = data?.getStringExtra("location")
            }
        }
    }

}
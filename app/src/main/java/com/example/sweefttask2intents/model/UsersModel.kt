package com.example.sweefttask2intents.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UsersModel(val firstname: String, val lastname: String, val age: Int) : Parcelable

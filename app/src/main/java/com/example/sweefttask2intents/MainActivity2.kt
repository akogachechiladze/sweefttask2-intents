package com.example.sweefttask2intents


import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.sweefttask2intents.databinding.ActivityMain2Binding

class MainActivity2 : AppCompatActivity() {

    private lateinit var binding: ActivityMain2Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMain2Binding.inflate(layoutInflater)
        setContentView(binding.root)
        collectAndSet()
        initListeners()
    }

    private fun collectAndSet() {
        val firstname = intent.getStringExtra("firstname")
        val lastname = intent.getStringExtra("lastname")
        val age = intent.getIntExtra("age", 0)
        binding.firstname.text = firstname
        binding.lastname.text = lastname
        binding.age.text = age.toString()

        //second way
//        val user = intent.getParcelableExtra<UsersModel>("userModel")
//        binding.firstname.text = user?.firstname
//        binding.lastname.text = user?.lastname
//        binding.age.text = user?.age.toString()
    }

    private fun initListeners() {
        binding.back.setOnClickListener {
            val location = binding.location.text.toString()
            intent.putExtra("location", location)
            setResult(RESULT_OK, intent)
            finish()
        }
    }

}